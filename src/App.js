import Hero from "./components/Hero";
import Navigation from "./components/Navigation";
import Skills from "./components/Skills";
import Experience from "./components/Experience";
import Projects from "./components/Projects";
import Education from "./components/Education";
import Contact from "./components/Contact";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
      <Navigation id="Navigation" />
      <Hero
        id="Hero"
        name="Joseph Haddad"
        title="Full Stack Engineer"
        intro="Welcome to my online portfolio! Here you will find my up to date skills as well as any current and past projects and experiences."
      />

      <Skills id="Skills" />
      <Experience id="Experience" />
      <Projects id="Projects" />
      <Education id="Education" />
      <Contact id="Contact" />
      <Footer id="Footer" />
    </div>
  );
}

export default App;
