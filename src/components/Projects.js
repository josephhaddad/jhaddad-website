import { Element } from "react-scroll";
import {
  ExternalLinkIcon,
  QuestionMarkCircleIcon,
} from "@heroicons/react/outline";

const projects = [
  {
    title: "Guildford College",
    description:
      "Designed and built their website/webapp for student enrollments and content management.",
    status: "Active",
    tags: ["Education", "WebApps"],
    href: "https://guildfordcollege.com",
    logo: "img/guildford-college.svg",
  },
  {
    title: "Essential Flex",
    description: "E-commerce business for affordable wireless earbuds.",
    status: "Inactive",
    tags: ["E-commerce"],
    href: "",
    logo: "img/essential-flex.svg",
  },
  {
    title: "Project Syncio",
    description:
      "Project Syncio began in Victoria British Columbia by a group of students enrolled at the University of Victoria to provide more secure ways to automate your home.",
    status: "Inactive",
    tags: ["IoT", "WebApps", "Home Automation"],
    href: "https://projectsyncio.jhaddad.ca",
    logo: "img/project-syncio.svg",
  },
  {
    title: "AlgoBots",
    status: "Active",
    description:
      "A strategy trading platform for Cryptocurrencies that allows the user to implement algorithms directly in the UI and activate a bot to run the strategies.",
    tags: ["Finance", "Trading", "WebApps"],
    href: "",
    logo: "",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const Projects = (props) => {
  return (
    <Element name={props.id}>
      <div className="max-w-7xl mx-auto text-center py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
        <h2 className="text-xl text-indigo-600 font-semibold tracking-wide uppercase">
          Projects
        </h2>
        <p className="mt-2 text-3xl font-extrabold text-gray-900">
          Production Projects
        </p>
        <p className="mt-3 text-base font-medium text-gray-600 sm:mt-3 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-3 md:text-xl">
          Here you can find my personal, degree and professional projects
        </p>
        <div className="max-w-5xl mx-auto mt-5">
          <div className="rounded-lg bg-gray-200 overflow-hidden shadow divide-y divide-gray-200 sm:divide-y-0 sm:grid sm:grid-cols-2 sm:gap-px">
            {projects.map((project, projectIdx) => (
              <div
                key={project.title}
                className={classNames(
                  projectIdx === 0
                    ? "rounded-tl-lg rounded-tr-lg sm:rounded-tr-none"
                    : "",
                  projectIdx === 1 ? "sm:rounded-tr-lg" : "",
                  projectIdx === projects.length - 2 ? "sm:rounded-bl-lg" : "",
                  projectIdx === projects.length - 1
                    ? "rounded-bl-lg rounded-br-lg sm:rounded-bl-none"
                    : "",
                  "relative group bg-white p-6 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-500"
                )}
              >
                {/* <p className="mx-1 px-3 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-400">
                                    {project.status}
                                </p> */}
                <span
                  className={classNames(
                    project.status == "Active"
                      ? "bg-green-200 text-green-600"
                      : "bg-gray-300 text-gray-600",
                    "my-2 mx-1 px-3 inline-flex text-xs leading-5 font-semibold rounded-full"
                  )}
                  aria-hidden="true"
                >
                  {project.status}
                </span>

                <div className="flex justify-center">
                  {project.logo ? (
                    <img className="w-72 h-20" src={project.logo} />
                  ) : (
                    <QuestionMarkCircleIcon
                      className="h-20 text-gray-400"
                      aria-hidden="true"
                    />
                  )}
                </div>

                <div className="">
                  <div className="mt-5">
                    <h3 className="text-lg font-medium">
                      {project.href ? (
                        <a
                          href={project.href}
                          target="_blank"
                          className="focus:outline-none"
                        >
                          {/* Extend touch target to entire panel */}
                          <span
                            className="absolute inset-0"
                            aria-hidden="true"
                          />
                          {project.title}
                        </a>
                      ) : (
                        <a className="focus:outline-none">
                          {/* Extend touch target to entire panel */}
                          <span
                            className="absolute inset-0"
                            aria-hidden="true"
                          />
                          {project.title}
                        </a>
                      )}
                    </h3>
                    <p className="my-3 text-sm text-gray-500">
                      {project.description}
                    </p>
                    <div className="flex justify-center mt-10">
                      <div className="flex-shrink-0 absolute bottom-5">
                        {project.tags.map((tag, tagIdx) => (
                          <p className="mx-1 px-3 inline-flex text-xs leading-5 font-semibold rounded-full bg-indigo-100 text-indigo-700">
                            {tag}
                          </p>
                        ))}
                      </div>
                    </div>
                  </div>
                  {project.href ? (
                    <span
                      className="pointer-events-none absolute top-6 right-6 text-gray-300 group-hover:text-indigo-500"
                      aria-hidden="true"
                    >
                      <ExternalLinkIcon
                        className="h-6 w-6"
                        aria-hidden="true"
                      />
                    </span>
                  ) : (
                    <span></span>
                  )}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Element>
  );
};

export default Projects;
