import { SocialIcon } from "react-social-icons";
import { Element } from "react-scroll";

const Social = (props) => {
  return (
    <Element name={props.id}>
      <div className="mt-8 space-x-3">
        <SocialIcon
          fgColor="white"
          url="https://www.linkedin.com/in/joseph-haddad-91915b140/"
          target="_blank"
        />
        <SocialIcon
          fgColor="white"
          url="https://twitter.com/jhadda_d"
          target="_blank"
        />
      </div>
    </Element>
  );
};

export default Social;
