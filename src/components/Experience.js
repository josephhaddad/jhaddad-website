import { Element } from "react-scroll";
import {
  LocationMarkerIcon,
  UsersIcon,
  CalendarIcon,
  SparklesIcon,
  BriefcaseIcon,
} from "@heroicons/react/outline";

const experiences = [
  {
    id: 1,
    title: "Full Stack Engineer",
    type: "Full-time",
    location: "Surrey, British Columbia",
    company: "Guildford College",
    startDate: "Jan 2020",
    endDate: "Dec 2021",
  },
];

const Experience = (props) => {
  return (
    <Element name={props.id}>
      <div className="relative bg-white pt-16 pb-32 overflow-hidden">
        <div className="relative"></div>
        <div className="mt-24">
          <div className="lg:mx-auto lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-2 lg:grid-flow-col-dense lg:gap-24">
            <div>
              <h2 className="text-xl font-semibold text-indigo-600 uppercase tracking-wide">
                Experience
              </h2>
              <p className="mt-2 text-3xl font-extrabold text-gray-900">
                My Professional Experience
              </p>
              <p className="mt-3 text-lg text-gray-600">
                From startups to large coorporations.
              </p>
            </div>
            <div className="mt-12 sm:mt-16 lg:mt-0 lg:col-start-1">
              <div className="pr-4 -ml-48 sm:pr-6 md:-ml-16 lg:px-0 lg:m-0 lg:relative lg:h-full">
                <div className="bg-white shadow overflow-hidden sm:rounded-md">
                  <ul role="list" className="divide-y divide-gray-200">
                    {experiences.map((experience) => (
                      <li key={experience.id}>
                        <a className="block hover:bg-gray-50">
                          <div className="px-4 py-4 sm:px-6">
                            <div className="flex items-center justify-between">
                              <p className="text-sm font-medium text-indigo-600 truncate">
                                {experience.title}
                              </p>
                              <div className="ml-2 flex-shrink-0 flex">
                                <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                  {experience.type}
                                </p>
                              </div>
                            </div>
                            <div className="mt-2 sm:flex sm:justify-between">
                              <div className="sm:flex">
                                <p className="flex items-center text-sm text-gray-500">
                                  <BriefcaseIcon
                                    className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                                    aria-hidden="true"
                                  />
                                  {experience.company}
                                </p>
                                <p className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0 sm:ml-6">
                                  <LocationMarkerIcon
                                    className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                                    aria-hidden="true"
                                  />
                                  {experience.location}
                                </p>
                              </div>
                              <div className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0">
                                <CalendarIcon
                                  className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                                  aria-hidden="true"
                                />
                                <p>
                                  <time dateTime={experience.startDate}>
                                    {experience.startDate}
                                  </time>
                                  -
                                  <time dateTime={experience.endDate}>
                                    {experience.endDate}
                                  </time>
                                </p>
                              </div>
                            </div>
                          </div>
                        </a>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Element>
  );
};

export default Experience;
