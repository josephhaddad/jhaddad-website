import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import { MenuIcon, XIcon } from '@heroicons/react/outline'
import { Link, Element, animateScroll as scroll } from "react-scroll";

import Social from './Social';



const Hero = (props) => {
  return (
    <Element name={props.id} id={props.id}>
    <div className="relative bg-gradient-to-r from-cyan-100 to-indigo-400 overflow-hidden">
      <div className="max-w-7xl mx-auto">
        <div className="relative z-10 pb-0 bg-transparent lg:w-full">
          
          <main className="mt-10 mx-auto max-w-7xl pb-10 px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28 sm:pb-16 md:pb-20 lg:pb-28 xl:pb-32">             
            <div className="text-center sm:text-center lg:text-left">
              <h1 className="text-5xl tracking-tight font-extrabold text-gray-900 sm:text-6xl md:text-7xl">
                <span className="block xl:inline">{props.name}</span>
              </h1>
              <h1 className="text-4xl tracking-tight font-extrabold text-gray-900 sm:text-6xl md:text-7xl">
                <span className="block mt-2 text-indigo-600 xl:inline sm:text-4xl md:text-5xl">{props.title}</span>
              </h1>
              <p className="mt-3 text-base font-medium text-gray-700 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
                {props.intro}
              </p>
              
              <Social id="Social"/>
              
              <div className="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
                <div className="rounded-md shadow">
                  <Link 
                    href='#'
                    className="w-full flex items-center justify-center px-8 py-2 border border-transparent text-base font-medium rounded-md text-white bg-indigo-500 hover:bg-indigo-600 md:py-2 md:text-lg md:px-10"
                    activeClass="active"
                    to="Skills"
                    spy={true}
                    smooth={true}
                    offset={-30}
                    duration={500}
                  >
                    Learn more
                  </Link>
                </div>

              </div>
            </div>
          </main>
          <div className="w-1/2 h-100 absolute bottom-0 left:0 lg:w-100 lg:h-full lg:inset-y-0 lg:right-0 -z-10">
                <img
                className="object-cover h-full w-full"
                src="hero.png"
                alt=""
                />
            </div>
        </div>
      </div>
    </div>
    </Element>
  )
};

export default Hero;