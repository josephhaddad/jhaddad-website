import { Element } from "react-scroll";
import { CheckIcon, QuestionMarkCircleIcon } from "@heroicons/react/outline";

const skills = [
  {
    name: "HTML",
  },
  {
    name: "CSS",
  },
  {
    name: "Bootstrap",
  },
  {
    name: "Tailwind CSS",
  },
  {
    name: "Javascript - React",
  },
  {
    name: "Python",
  },
  {
    name: "Django - DRF",
  },
  {
    name: "PostgreSQL",
  },
  {
    name: "Docker",
  },
  {
    name: "Nginx",
  },
  {
    name: "Traefik",
  },
  {
    name: "Celery",
  },
  {
    name: "Git",
  },
];

const Skills = (props) => {
  return (
    <Element name={props.id}>
      <div className="bg-white">
        <div className="relative max-w-7xl mx-auto py-16 px-4 sm:px-6 lg:py-24 lg:px-8 lg:grid lg:grid-cols-3 lg:gap-x-8">

          <div>
            <h2 className="text-xl font-semibold text-indigo-600 uppercase tracking-wide">
              Skills
            </h2>
            <p className="mt-2 text-3xl font-extrabold text-gray-900">
              Web Development Skills
            </p>
            <p className="mt-3 text-lg text-gray-600">
              From classic html and css to web frameworks and development
              operations.
            </p>
          </div>
          <div className="mt-12 lg:mt-8 lg:col-span-2">
            <dl className="space-y-5 lg:space-y-0 lg:grid lg:grid-cols-3 lg:gap-5">
              {skills.map((skill) => (
                <div key={skill.name} className="relative">
                  <dt>
                    <CheckIcon
                      className="absolute h-6 w-6 text-green-500"
                      aria-hidden="true"
                    />
                    <p className="ml-9 text-lg leading-5 font-medium text-gray-900">
                      {skill.name}
                    </p>
                  </dt>
                </div>
              ))}
            </dl>
          </div>
        </div>
      </div>
    </Element>
  );
};

export default Skills;
