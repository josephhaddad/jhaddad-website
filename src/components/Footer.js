import { Element } from "react-scroll";

const Footer = (props) => {
    return (
        <Element name={props.id}>
            <footer className="bg-white" aria-labelledby="footer-heading">
                <div className="mt-12 border-t border-warm-gray-200 py-4">
                    <p className="text-base text-warm-gray-400 text-center">
                        Designed &amp; Built by Joseph Haddad
                    </p>
                </div>
            </footer>
        </Element>
    )
};
    
export default Footer;